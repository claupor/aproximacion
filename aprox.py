#!/usr/bin/python3 

import sys


def filter_args():
    for i in sys.argv[2:]:
        if i.isdigit() == False:
           print("All arguments must be integer")
           sys.exit(1)

def compute_trees(trees):

    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit
    return production

def compute_all(min_trees, max_trees):

    productions = []

    for item in range(min_trees, max_trees+1):
        productions.append((item, compute_trees(item)))

    return productions

def read_arguments():

    base_trees = int(sys.argv[1])
    fruit_per_tree = int(sys.argv[2])
    reduction = int(sys.argv[3])
    min = int(sys.argv[4])
    max = int(sys.argv[5])


    return base_trees, fruit_per_tree, reduction, min, max


def main():
    filter_args()
    global base_trees
    global fruit_per_tree
    global reduction
    base_trees, fruit_per_tree, reduction, min, max = read_arguments()
    productions = compute_all(min, max)
    best_production = 0
    best_trees = 0
    for prod in productions:
        if best_production < prod[1]:
            best_production = prod[1]
            best_trees = prod[0]
        print(f"{prod[0]} {prod[1]}")
    print(f"Best production: {best_production}, for {best_trees} trees")


if __name__ == '__main__':
    if len(sys.argv) != 6:
        print("Usage: python3 aprox.py <base_trees> <fruit_per_tree> <reduction> <min> <max>")
    else:
        main()

